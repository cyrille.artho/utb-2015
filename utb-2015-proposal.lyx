#LyX 2.1 created this file. For more info see http://www.lyx.org/
\lyxformat 474
\begin_document
\begin_header
\textclass IEEEtran
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_math auto
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Validating Software Tests (VST)
\end_layout

\begin_layout Author
Cyrille Artho, Eun-Hye Choi, Takashi Kitamura
\begin_inset Newline newline
\end_inset

National Institute of Advanced Industrial Science and Technology (AIST)
\begin_inset Newline newline
\end_inset

Midorigaoka 1-8-31 Ikeda Osaka 563-8577 Japan
\begin_inset Newline newline
\end_inset

{c.artho,e.choi,t.kitamura}@aist.go.jp
\end_layout

\begin_layout Abstract
Large software projects tend to accumulate many test cases, which can become
 difficult to maintain.
 Both automatically-generated and human-written test cases often come without
 documentation, so it is difficult to determine how a test should be adapted
 to a new system.
\end_layout

\begin_layout Abstract
Should a passing test perhaps fail? Should a failing test be made to pass?
 Are there tools and techniques that can help us find an answer to these
 questions?
\end_layout

\begin_layout Abstract
Unlike full program understanding and visualization, understanding a test
 case typically involves a single program trace, and is therefore a more
 target and tractable problem.
 Much work has been done on individual aspects of this problem, and we hope
 this workshop gives a platform for discussions and possible synergistic
 approaches.
\end_layout

\begin_layout Section
Organizer's background, past experience
\end_layout

\begin_layout Subsection
Cyrille Artho
\end_layout

\begin_layout Subsubsection*
Bio
\end_layout

\begin_layout Standard
Cyrille Artho's main interests are software verification and software engineerin
g.
 In his master's thesis, he compared different approaches for finding faults
 in multi-threaded programs.
 Artho obtained his Ph.D.
 at ETH Zurich in 2005.
 In that work, he investigated different approaches for finding faults in
 multi-threaded programs.
 In particular, in joint work with NASA Ames he found that high-level data
 races can show potential problems in concurrent software even if individual
 data accesses are safe.
\end_layout

\begin_layout Standard
After obtaining his Ph.D., Artho moved to Tokyo, where he worked for two years
 at the National Institute of Informatics as a Postdoctoral Researcher.
 From April 2007 onwards, he worked at the AIST, where he currently works
 as Senior Researcher.
\end_layout

\begin_layout Standard
In recent work, he extended his work on concurrent software to networked
 programs.
 To analyze concurrency exhaustively, he lead the development of the Java
 PathFinder extension "net-iocache", so networked software can be used in
 that tool.
 Furthermore, he developed his own model-based test tool Modbat, which is
 specialized for generating tests for networked software.
\end_layout

\begin_layout Subsubsection*
Past experience
\end_layout

\begin_layout Standard
Artho has been workshop co-chair (together with Peter Ölveczky) of the Internati
onal Workshop on Formal Techniques for Safety-Critical Systems (FTSCS),
 a very successful workshop series that has been running from 2012--2015
 so far, attracting over 40 submissions in the last two years.
 He was also on the program committee of numerous international conferences
 and workshops, including LPAR, FACS, ICECCS, and other events in 2015 and
 earlier.
\end_layout

\begin_layout Subsection
Eun-Hye Choi
\end_layout

\begin_layout Subsubsection*
Bio and Past experience
\end_layout

\begin_layout Standard
Eun-Hye Choi's main interests are software verification and software engineering.
 Choi worked as a doctoral-course research fellow of JSPS (Japan Society
 for the Promotion of Science) from 1999 to 2002 and obtained her Ph.D.
 at Osaka University in 2002.
 In her Ph.D.
 thesis, she researched on design and evaluation of dependable distributed
 mutual exclusion algorithms.
 
\end_layout

\begin_layout Standard
After obtaining her Ph.D., she worked at Toshiba corporation and investigated
 a high-performance concurrency control mechanism for an XML database.
 
\end_layout

\begin_layout Standard
From April 2004 onwards, she has been working at AIST and studied on software
 verification, applying model-checking to various applications such as a
 web application, an active database, and a distributed commitment protocol.
 She also developed a formal specification language for embedded network
 systems and a specification-based test generation.
 She is currently working on prioritized combinatorial test generation and
 evaluation.
 
\end_layout

\begin_layout Standard
Choi was on the program committee of International Conference on Software
 Engineering Research, Management and Applications (SERA 2014).
\end_layout

\begin_layout Subsection
Takashi Kitamura
\end_layout

\begin_layout Subsubsection*
Bio and Past experience
\end_layout

\begin_layout Standard
Dr.
 Takashi Kitamura is a researcher at AIST.
 His main research interests are software engineering, particularly software
 testing and verification.
 He obtained his Ph.D.
 at the Chinese Academy of Sciences, Beijing in 2008.
 In that work, he developed an algebraic and logical framework of specification
 and verification for distributed and mobile systems.
 After joining AIST, his research work focuses on technology transfer as
 well as theoretical researches of software testing and verification, in
 collaboration with various companies.
 From such activities, he published a number of publications in top-tier
 conferences, including ICST 2015, QSIC 2015, etc., and developed several
 software engineering tools.
 
\end_layout

\begin_layout Standard
Kitamura has received the Yamashita Award from the Information Processing
 Society of Japan (IPSJ) in 2011 and the Best Paper Award of QSIC 2015.
 He has served on various program committees, such as PRDC 2015, FTSCS 2012--201
5, and the editorial committee of the Journal of Information Processing
 (JIP).
\end_layout

\begin_layout Standard
\begin_inset Newpage pagebreak
\end_inset


\end_layout

\begin_layout Section
Workshop topics and goals
\end_layout

\begin_layout Standard
Our goal is to focus contributions on software reengineering from the main
 conference, to the specific task of understanding, maintaining, and evolving
 software tests.
 This gives rise to more specific problems and topics, such as:
\end_layout

\begin_layout Itemize
Test minimization and simplification
\end_layout

\begin_layout Itemize
Fault localization
\end_layout

\begin_layout Itemize
Test visualization
\end_layout

\begin_layout Itemize
Change analysis for software tests
\end_layout

\begin_layout Itemize
Test validation
\end_layout

\begin_layout Itemize
Documentation analysis
\end_layout

\begin_layout Itemize
Bug report analysis
\end_layout

\begin_layout Itemize
Test evolution
\end_layout

\begin_layout Itemize
Test case generation, including random testing, symbolic execution, etc.
\end_layout

\begin_layout Itemize
Model-based testing
\end_layout

\begin_layout Itemize
Combinations of the topics above
\end_layout

\begin_layout Section
Planned schedule
\end_layout

\begin_layout Standard
If we attract enough papers, we plan to have a full-day workshop, otherwise
 a half-day event.
 To have a more workshop-like character, we plan shorter presentations and
 combine discussions on similar issues in an ad-hoc panel at the end of
 each session (see below).
 A session will combine three presentations around a similar topic (20 minutes
 per presentation) with a 30-minute block for Q & A (5--10 minutes) and
 discussions.
 The first session (in the morning) will be slightly shorter, with two presentat
ions and a slightly shorter round of discussions (see Table
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand ref
reference "tab:Planned-schedule"

\end_inset

).
\end_layout

\begin_layout Standard
\begin_inset Float table
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Planned schedule
\begin_inset CommandInset label
LatexCommand label
name "tab:Planned-schedule"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset Tabular
<lyxtabular version="3" rows="4" columns="2">
<features rotate="0" tabularvalignment="middle">
<column alignment="right" valignment="top">
<column alignment="left" valignment="top">
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
9:00--10:00
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Session 1
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
10:30--12:00
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Session 2
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
13:30--15:00
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Session 3 (optional)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
15:30--17:00
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Session 4 (optional)
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
The proposed format is designed for an expected number of 8--10 submissions,
 out of which 4--5 are accepted.
 Depending on the quality and number of the submissions, we can scale the
 event up to a maximum of 9 accepted papers.
\end_layout

\begin_layout Section
Plans for generating and stimulating discussion at the workshop
\end_layout

\begin_layout Standard
We want to encourage submissions on work in progress, and make the presentations
 slightly shorter than typical conference presentations.
 Our focused topics should make it easy to group similar works together,
 so we can postpone the typical Q & A session after each presentation, to
 the end of a session of 2--3 presentations.
 This allows for an easier comparison between related works.
\end_layout

\begin_layout Standard
After questions that clarify the contents of the presentations, we plan
 to initiate the discussion in a panel discussion style.
 The discussion will start with an opening question (from the session chair
 or the audience) that concerns all papers of a given session, allowing
 each presenter to state his or her point of view from that perspective.
 After that we will have an open format, where the audience or session chair
 can ask questions as the discussion evolves.
\end_layout

\begin_layout Section
Expected outcomes and follow-ups of the workshop
\end_layout

\begin_layout Standard
We hope to attract many high-quality papers and have interesting and lively
 presentations and discussions at the workshop.
 If we attract a large number of high-quality submissions, we will consider
 running the workshop again in 2017.
\end_layout

\begin_layout Standard
On the practical side, we hope the workshop will have impact on the focused
 topic of understanding the behavior and outcome of single software executions
 (test cases).
 This topic may allow for more targeted approaches than more generic problems
 in software understanding, which are handled by large conferences.
 We also hope that the workshop gives rise to synergies and collaborations,
 which are often easier when there is a well-defined and tractable problem
 at hand.
\end_layout

\begin_layout Section
Paper selection criteria and mechanism
\end_layout

\begin_layout Standard
We plan an open call for papers, calling for submissions on the topics mentioned
 above with a page limit of four pages, IEEE format.
 Papers will be selected based on scientific originality, novelty, and the
 potential to generate interesting discussions.
\end_layout

\begin_layout Standard
Due to the schedule given by SANER,
\begin_inset Foot
status open

\begin_layout Plain Layout
Workshop proposals are accepted in mid November.
\end_layout

\end_inset

 it is not possible to have formally published pre-proceedings for the workshop.
 We therefore aim for having informal pre-proceedings and ask for a camera-ready
 version a week after the conference.
 Due to the brevity of the papers, we aim at a fast review cycle, which
 gives us the following schedule:
\end_layout

\begin_layout Itemize
Paper
\begin_inset space ~
\end_inset

submission: January 11, 2016 (will be extended to the end of the week, but
 abstract submissions will be due by Jan.
 11).
\end_layout

\begin_layout Itemize
Notification: February 11, 2016.
\end_layout

\begin_layout Standard
This schedule is similar to the one we use for FTSCS, which has been very
 successful in the last few years.
\end_layout

\begin_layout Subsection*
Program Committee (tentative):
\end_layout

\begin_layout Itemize
Cyrille Artho, AIST, Japan (co-chair)
\end_layout

\begin_layout Itemize
W.
\begin_inset space \space{}
\end_inset

K.
\begin_inset space \space{}
\end_inset

Chan, City University of Hong Kong
\end_layout

\begin_layout Itemize
Eun-Hye Choi, AIST, Japan (co-chair)
\end_layout

\begin_layout Itemize
Falk Howar, Technical University Clausthal, Germany
\end_layout

\begin_layout Itemize
Eunkyoung Jee, KAIST, South Korea
\end_layout

\begin_layout Itemize
Takashi Kitamura, AIST, Japan (co-chair)
\end_layout

\begin_layout Itemize
Huai Liu, RMIT University, Australia
\end_layout

\begin_layout Itemize
Teng Long, University of Maryland, USA
\end_layout

\begin_layout Itemize
Lei Ma, Chiba University, Japan
\end_layout

\begin_layout Itemize
Andrea Mocci, University of Lugano, Switzerland
\end_layout

\begin_layout Itemize
Rudolf Ramler, SCCH, Austria
\end_layout

\begin_layout Itemize
Martina Seidl, Johannes Kepler University, Austria
\end_layout

\end_body
\end_document
